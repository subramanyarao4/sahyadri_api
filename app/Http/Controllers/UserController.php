<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $validator  = Validator::make($request->all(),[
            'name'=>'required|max:255',
            'email'=>'required|max:255',
            'mobileNumber'=>'required|max:255',
            'usn'=>'required|max:10',
            'branch'=>'required|in:CSE,ECE,ME,ISE,CV,MBA',
            'gender'=>'required|max:255|in:Male,Female,Others',
            'sem'=>'required|max:255',
            'password'=>'required|min:10',
            'token'=>'required|max:255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        $user = new User();
        $user->userId = Uuid::uuid4();
        $user->name = $request->get("name");
        $user->email = $request->get("email");
        $user->password = $request->get("password");
        $user->usn = $request->get('usn');
        $user->branch = $request->get('branch');
        $user->sem = $request->get('sem');
        $user->mobileNumber = $request->get('mobileNumber');
        $user->firebaseUid = $request->get('firebaseUid');
        $user->token=$user->getJWTIdentifier();
        $user->save();
        return response()->json(["data"=>$user],200);
    }

    /**
     * Display a resources
     * @param User $user
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
