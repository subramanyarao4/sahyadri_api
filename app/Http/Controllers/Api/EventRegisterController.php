<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event_Register;
use Illuminate\Support\Facades\Validator;

class EventRegisterController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $register = Event_Register::all();
        return response()->json(['data' => $register], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required|max:255',
                'event_id' => 'required|max:255',
                'name' => 'required|max:255',
                'college_name' => 'required|max:255',
                'mobile_number' => 'required|max:10',
                'email_address' => 'required|max:255',
                'is_paid' => 'default:1'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            $register = Event_Register::where('user_id', '=', $request['user_id'])->get();
            if (count($register) == 0) {

                /**
                 * User event register
                 */

                $project_register = new Event_Register();
                $project_register->user_id = $request->get("user_id");
                $project_register->event_id = $request->get("event_id");
                $project_register->name = $request->get("name");
                $project_register->college_name = $request->get("college_name");
                $project_register->email_address = $request->get('email_address');
                $project_register->mobile_number = $request->get('mobile_number');
                $project_register->is_paid = $request->get('is_paid');
                $project_register->save();

                /**
                 * Send response back to user
                 */

                return response()->json(204);
            }
            return response()->json(["data" => "User already register."], 409);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage(), "code" => $e->getCode()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
