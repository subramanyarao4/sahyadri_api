<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Initial_Survey extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $initial_questions = [
            [
                'question_id'=>\Ramsey\Uuid\Uuid::uuid4(),
                'question_title'=> "Select your domain",
                'question_options'=>json_encode(
                    [
                        0=>"ML",
                        1=>"IOT",
                        2=>"Mobile Application",
                        3=>"AR/VR",
                        4=>"Web development"
                    ]
                )
            ],
            [
                'question_id'=>\Ramsey\Uuid\Uuid::uuid4(),
                'question_title'=> "What mode of learning do you prefer?",
                'question_options'=>json_encode(
                    [
                        0=>"Online",
                        1=>"Offline",
                    ]
                )
            ],
            [
                'question_id'=>\Ramsey\Uuid\Uuid::uuid4(),
                'question_title'=> "Should we provide with online courses?",
                'question_options'=>json_encode(
                    [
                        0=>"Yes",
                        1=>"No",
                    ]
                )
            ],
            [
                'question_id'=>\Ramsey\Uuid\Uuid::uuid4(),
                'question_title'=> "What are the constraints that restricts you?",
                'question_options'=>json_encode(
                    [
                        0=>"Lack of support",
                        1=>"Do not know where to go",
                        2=>"Nakkan sari illa"
                    ]
                )
            ],
        ];
        DB::table('initial_survey_questions')->insert($initial_questions);
    }
}
