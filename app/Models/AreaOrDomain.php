<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AreaOrDomain extends Model
{
    use Notifiable;
    protected  $table = 'areas_or_domains';

    protected $fillable = [
        'area_name'
    ];
}
