<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProjectCollaboration extends Model
{
    use Notifiable;
    protected $table = 'project_groups';
}
