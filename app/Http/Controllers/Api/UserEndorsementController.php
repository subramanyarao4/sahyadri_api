<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserEndorsement;
use Illuminate\Support\Facades\Validator;

class UserEndorsementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =Validator::make($request->all(), [
            "user_id" => "required|max:255",
            "endorser_id"=>"required|max:255",
            "linkedin_profile"=>"required|max:255",
            "message"=>"required|max:255",
            "know_about"=>"nullable",
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()],400);
        }

        $endorse = new UserEndorsement();
        $endorse->user_id = $request->get('user_id');
        $endorse->endorser_id = $request->get("endorser_id");
        $endorse->linkedin_profile = $request->get("linkedin_profile");
        $endorse->message = $request->get('message');
        $endorse->know_about = $request->get("know_about");
        $endorse->created_at = \Carbon\Carbon::now()->timestamp;
        $endorse->updated_at = \Carbon\Carbon::now()->timestamp;
        $endorse->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $endorse = UserEndorsement::where("user_id",'=',$id)->get();
        return response()->json(["data"=>$endorse],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
