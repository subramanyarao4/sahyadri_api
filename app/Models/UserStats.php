<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserStats extends Model
{
    use Notifiable;
    protected $table = "user_stats";
}
