<?php

namespace App\Http\Controllers\Api;

use App\Models\Feed;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $time = \Carbon\Carbon::now()->timestamp;
        $result = Feed::all();
        return response()->json(['data'=>json_encode($result)],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
          $validator =  Validator::make([
             'group_id'=>'require|max:255',
             'user_id'=>'require|max:255',
             'feed_id'=>'require|max:255',
             'feed_title'=>'require|max:255',
             'feed_desc'=>'require|max:255',
             'feed_conducted_by'=>'require|max:255',
             'feed_date'=>'require',
             'feed_start_time'=>'require',
             'feed_end_time'=>'require',
             'feed_images'=>'default:null',
             'feed_files'=>'default:null'
          ]);

          if($validator->fails()){
              return response()->json(['error'=>$validator->errors()],500);
          }
          $feed = new Feed();
          $feed->group_id = $request->get('group_id');
          $feed->user_id = $request->get('user_id');
          $feed->feed_id = $request->get('feed_id');
          $feed->feed_title = $request->get('feed_title');
          $feed->feed_desc = $request->get('feed_desc');
          $feed->feed_conducted_by = $request->get('feed_conducted_by');
          $feed->feed_date = $request->get('feed_date');
          $feed->feed_start_time = $request->get('feed_start_time');
          $feed->feed_end_time = $request->get('feed_end_time');
          $feed->feed_images = $request->get('feed_images');
          $feed->feed_files = $request->get('feed_files');
          $feed->save();
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage(),'code'=>$e->getCode()],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
