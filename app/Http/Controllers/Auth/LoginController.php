<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Factory;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $factory = (new Factory)
            ->withServiceAccount(base_path().'/firebase-adminsdk.json');
        $auth = $factory->createAuth();
        try {
            $data  = $auth->verifyPassword($request['email'], $request['password']);
            $customToken = $factory->createAuth()->createCustomToken($data->uid);
            echo $customToken;
            return [];

        } catch (AuthException $e) {
            print_r($e);
        } catch (FirebaseException $e) {
            print_r($e);
        }



//         print_r($request);
//        if (is_numeric($request->get('email'))) {
//            return ['phone' => $request->get('email'), 'password' => $request->get('password')];
//        } else if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
//            return $request->only($this->username(), 'password');
//        } else { //DDS ID
//            return ['uid' => $request->get('email'), 'password' => $request->get('password')];
//        }
    }
}
