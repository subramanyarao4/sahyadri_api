<?php

namespace App\Http\Controllers\Api;

use App\Models\LeaderBoard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeaderBoardController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $leaders = DB::table("user_details")->join('user_stats', 'user_details.user_id', '=', 'user_stats.user_id')->orderBy('total')->get();
        return response()->json(["data" => $leaders]);
        // $data = LeaderBoard::all();
        // return response()->json(["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
    }

    /**
     * Display a resources
     * @param $id
     */
    public function show($id)
    {
        $leader = LeaderBoard::where("user_id",'=',$id)->get();
        return response()->json(["data" => $leader]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
