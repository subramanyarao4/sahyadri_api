<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$router = app('Dingo\Api\Routing\Router');

$router->version('v1', ['namespace' => 'App\Http\Controllers\Api\Auth',], function ($api) {
    $api->group(['prefix' => 'auth'], function ($api) {
        $api->post('/token', ['as' => 'auth.token', 'uses' => 'TokenController@authenticate']);
    });
});

/**
 * Resource access
 */
$router->version('v1',['namespace' => 'App\Http\Controllers\Api','middleware' => 'auth:api'] , function ($api){
    $api->resource("users",'UserController');
    $api->resource("events","EventController");
    $api->resource("areas","AreasController");
    $api->resource("projects","ProjectController");
    $api->resource("feeds","FeedController");
    $api->resource("leader",'LeaderBoardController');
    $api->resource("event_register","EventRegisterController");
    $api->resource('survey','InitialQuestionController');
    $api->resource('survey_response','InitialSurveyResponse');
    $api->resource('notify','NotificationRequestController');
});




/**
 * register
 */
$router->version('v1',['namespace'=>'App\Http\Controllers\Api'],function ($api){
    $api->resource('studentRegister','UserController');
});

$router->version('v1',['namespace'=>'App\Http\Controllers\Api'],function ($api){
    $api->post('login','StudentLoginController@store');
});

/**
 * Admin APIs
 */
/**
 * Event Generation
 */
$router->version('v1',['namespace'=>'App\Http\Controllers\Api'],function ($api){
   $api->group(['prefix'=>'admin'],function($api){
       $api->resource('events','EventController');
   });
});



