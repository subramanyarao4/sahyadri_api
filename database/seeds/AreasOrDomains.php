<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreasOrDomains extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            'Machine Learning',
            'IOT',
            'Mobile Application Dev',
            'Web Development',
            'AR/VR',
            '3D-Modeling',
            'Game Dev',
            'Database Management',
            'Block Chain',
            'Data Analytics',
            'Content Writing'
        ];
        DB::table('areas_or_domains')->insert($areas);
    }
}
