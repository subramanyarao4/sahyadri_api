<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Notification_Request extends Model
{
    use Notifiable;
    protected $table = "notification_request";
}
