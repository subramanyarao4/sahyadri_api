<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = "feed_table";
    protected $fillable = [
        "group_id", "user_id", "feed_title", "feed_title", "feed_desc", "feed_conducted_by", "feed_date", "feed_start_time", "feed_end_time", "feed_images", "feed_files",
    ];
}
