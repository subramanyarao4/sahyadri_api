<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Contract\Auth\Provider;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Firebase\Auth\Token\Verifier;
use App\Models\User;
use Carbon\Carbon;
use DateTime;

class FirebaseAuthProvider implements Provider
{
    protected $verifier;
    public function __construct(Verifier $verifier)
    {
        $this->verifier = $verifier;
    }
    public function authenticate(Request $request, Route $route)
    {
        // Logic to authenticate the request.
        $token = $request->bearerToken();
        if ($token != null) {
            try {
                $token = $this->verifier->verifyIdToken($token);
                $userID = $token->getClaim('user_id');
                $phoneNumber = $token->getClaim('phone_number');
                $user = User::where('phone', $phoneNumber)->first();

                if ($user == null) {
                    //Create new User
                    $user = User::Create(
                        [
                            'phone' => $phoneNumber,
                            'firebase_uid' => $userID,
                        ]
                    );
                    $user->phoneVerifiedAt(Carbon::now());
                    $user->save();
                }
                auth()->login($user);

                $now = new Carbon();
                $user->last_login_at = $user->current_login_at == null ? $now : $user->current_login_at;
                $user->current_login_at = $now;
                $user->disableLogging();
                $user->save();

                return $user;
                // return new User($token->getClaims());
            } catch (\Exception $e) {
                // dd($e);
                throw new UnauthorizedHttpException('Unable to authenticate user.');
                return;
            }
        }
        throw new UnauthorizedHttpException('Unable to authenticate user.');
    }
}
