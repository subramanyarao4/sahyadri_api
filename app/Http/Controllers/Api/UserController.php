<?php

namespace App\Http\Controllers\Api;

use App\Models\UserDetails;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use function Psy\debug;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\UserEndorsement;
use App\Models\UserFollowing;
use App\Models\UserStats;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // $data = UserDetails::all();
        // return response()->json(["data"=>$data]);
         $userId =  Auth::user()->getAuthIdentifier();
         $data = UserDetails::where("firebaseUid","=",$userId)->get();
         $endorse = UserEndorsement::where('user_id','=',$data[0]['user_id'])->get();
         $following = UserFollowing::where('user_id','=',$data[0]['user_id'])->get();
         return response()->json(array("data"=>$data,"endorsement"=>count($endorse),"following"=>count($following)));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try{
            $validator  = Validator::make($request->all(),[
                'fullname'=>'required|max:255',
                'college_name'=>'required|max:255',
                'email_personal'=>'required|max:255',
                'mobile_number'=>'required|max:10',
                'branch'=>'required|in:CSE,ECE,ME,ISE,CV,MBA',
                'gender'=>'required|max:255|in:Male,Female,Others',
                'semester'=>'required|max:255|in:1,2,3,4,5,6,7,8|integer',
                'firebaseUid'=>'required|max:255'
            ]);
            if($validator->fails()){
                return response()->json(['error'=>$validator->errors()],400);
            }
            $valid = UserDetails::where('email_personal','=',$request->get('email_personal'))->get();
            if(count($valid)==0){

                /**
                 * User details
                 */

                $userID =  Uuid::uuid4();
                $userDetails = new UserDetails();
                $userDetails->user_id = $userID;
                $userDetails->group_id = '9d2c6505-59b5-41d7-94df-1acf320a95d1';
                $userDetails->fullname = $request->get("fullname");
                $userDetails->college_name = $request->get("college_name");
                $userDetails->email_personal = $request->get('email_personal');
                $userDetails->mobile_number = $request->get('mobile_number');
                $userDetails->branch = $request->get('branch');
                $userDetails->semester = $request->get('semester');
                $userDetails->gender = $request->get('gender');
                $userDetails->firebaseUid = $request->get('firebaseUid');
                $userDetails->save();

                /**
                 * User adding
                 */
                $user = new User();
                $user->user_id = $userID;
                $user->group_id = '9d2c6505-59b5-41d7-94df-1acf320a95d1';
                $user->created_at = \Carbon\Carbon::now()->timestamp;
                $user->updated_at = \Carbon\Carbon::now()->timestamp;
                $user->save();


                 /**
                 * User Stats Registeration
                 */
                $stats = new UserStats();
                $stats->user_id = $userID;
                $stats->Effective_Com = 0;
                $stats->Org_and_Man = 0;
                $stats->Negotiation = 0;
                $stats->Crit_Think = 0;
                $stats->Team_and_Dele = 0;
                $stats->Research_and_Anal = 0;
                $stats->Confidence = 0;
                $stats->total = 0;
                $stats->save();


                /**
                 * Send response back to user
                 */

                return response()->json(["data"=>$userDetails],200);
            }
            return response()->json(["data"=>"User already exists. Please try logging again"],400);
        }catch (\Exception $e){
            return response()->json(["error"=>$e->getMessage(),"code"=>$e->getCode()],500);
        }
    }

    /**
     * Display a resources
     * @param $id
     */
    public function show($id)
    {
        $user = UserDetails::where("user_id","=",$id)->get();
        $projects = Project::where("owner_id","=",$id)->get();
        return response()->json(['data'=>$user,'projects'=>$projects],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request,$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
