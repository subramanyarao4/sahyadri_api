<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification_Request;
use App\Models\ProjectCollaboration;
use App\Models\UserDetails;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class ProjectController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId =  Auth::user()->getAuthIdentifier();
        $dbId = UserDetails::where("firebaseUid", '=', $userId)->get('user_id');
        $project = Project::where('owner_id', '=', $dbId[0]['user_id'])->get();
        return response()->json(["data" => $project], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'proj_name' => 'required|max:255',
                'start_date' => 'required|max:255',
                'end_date' => 'required|nullable',
                'proj_desc' => 'required|max:255',
                'proj_domain' => 'required|nullable',
                'proj_tabs' => 'nullable',
                'proj_images' => 'nullable',
                'proj_file' => 'nullable',
                'awards' => 'nullable',
                'sector_id' => 'nullable',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            $project = new Project();
            $projId = Uuid::uuid4();
            $project->project_id = $projId;
            $project->proj_name = $request->get('proj_name');
            $project->start_date = $request->get('start_date');
            $project->end_date = $request->get('end_date');
            $project->proj_desc = $request->get('proj_desc');
            $project->proj_status = $request->get('proj_status');
            $project->proj_domain = $request->get('proj_domain');
            $project->proj_tabs = $request->get('proj_tabs');
            $project->proj_images = $request->get('proj_images');
            $project->proj_file = $request->get('proj_file');
            $project->awards = $request->get('awards');
            $project->claps = $request->get('claps');
            $project->sector_id = $request->get('sector_id');
            $project->proj_approval = $request->get('proj_approval');
            $project->owner_id = $request->get('owner_id');

            $project->save();


            /**
             * Adding colloborators
             */
            $collabsList = $request->get('collab');
            $collab = new ProjectCollaboration();
            for ($i = 0; $i < count($collabsList); $i++) {
                $collab->group_id = '9d2c6505-59b5-41d7-94df-1acf320a95d1';
                $collab->user_id =  $projId;
                $collab->user_id = $collabsList[$i];
                $collab->save();
            }

            return response()->json(['data' => $project], 200);
        } catch (\Exception $e) {
            return response()->json(['Error' => $e->getMessage(), 'code' => $e->getCode()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::where("project_id", "=", $id)->firstOrFail();
        return response()->json(["data" => $project], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $proj = Project::where("project_id","=",$id);
            $proj->delete();
            $project = new Project();
            $project->project_id = $id;
            $project->proj_name = $request->get('proj_name');
            $project->start_date = $request->get('start_date');
            $project->end_date = $request->get('end_date');
            $project->proj_desc = $request->get('proj_desc');
            $project->proj_status = $request->get('proj_status');
            $project->proj_domain = $request->get('proj_domain');
            $project->proj_tabs = $request->get('proj_tabs');
            $project->proj_images = $request->get('proj_images');
            $project->proj_file = $request->get('proj_file');
            $project->awards = $request->get('awards');
            $project->sector_id = $request->get('sector_id');
            $project->proj_approval = $request->get('proj_approval');
            $project->owner_id = $request->get('owner_id');
            $project->is_global = $request->get('is_global');
            $project->save();

            /**
             * Add Notification request if is_global == true;
             */

             if($request->get('is_global')){
                $notId = Uuid::uuid4();
                $notificationRequest = new Notification_Request();
                $notificationRequest->notification_id =  $notId ;
                $notificationRequest->request_user_id = $request->get('owner_id');
                $notificationRequest->title = $request->get('proj_name');
                $notificationRequest->desc =  $request->get('proj_desc');
                $notificationRequest->project_id = $id;
                $notificationRequest->created_at = \Carbon\Carbon::now()->timestamp;
                $notificationRequest->save();
             }

            return response()->json(["data" => $project], 200);
        } catch (\Exception $e) {
            return response()->json(['Error' => $e->getMessage(), 'code' => $e->getCode()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
