<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\Initial_Survey_Questions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InitialQuestionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Initial_Survey_Questions::all();
        return response()->json (['data'=>$questions],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                "question_id"=>'required|max:255',
                "question_title"=>'required|max:255',
                "question_options"=>'required'
            ]);
            if($validator->fails()){
                return response()->json(['error'=>$validator->errors()],400);
            }
            $initQuestion =  new Initial_Survey_Questions();
            $initQuestion->question_id = $request->get('question_id');
            $initQuestion->question_title = $request->get('question_title');
            $initQuestion->question_options = $request->get('question_options');
            $initQuestion->save();
            return response()->json(204);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage(),'code'=>$e->getCode()],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
