<?php

namespace App\Http\Controllers;

use App\User;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class UserControllerBackup extends Controller
{
    public function register(Request $request){

        $validator = Validator::make($request->json()->all(),[
            'name'=>'required|min:255',
            'email'=>'required|min:255',
            'mobileNumber'=>'required|min:255',
            'usn'=>'required|max:10',
            'branch'=>'required|in:CSE,ECE,ME,ISE,CV,MBA',
            'gender'=>'required|min:255|in:male,female,other',
            'sem'=>'required|min:255',
            'password'=>'required|min:10',
            'token'=>'required|min:255'
        ]);

        if($validator->fails()){
            return response()->json($validator->toJson(),400);
        }

        $user = User::create([
            'name'=>$request->json()->get('name'),
            'email'=>$request->json()->get('email'),
            'mobileNumber'=>$request->json()->get('mobileNumber'),
            'usn'=>$request->json()->get('usn'),
            'branch'=>$request->json()->get('branch'),
            'gender'=>$request->json()->get('gender'),
            'sem'=>$request->json()->get('sem'),
            'password'=>$request->json()->get('password'),
            'token'=>$request->json()->get('token'),

        ]);

        $token = JWTAuth::fromUser($user);
        return response()->json(compact('user','token'),201);
    }

    public function login(Request $request){
        $creds  = $request->json()->all();
        try {
            if(! $token = JWTAuth::attempt($creds)){
                return response()->json(['error'=>"Invalid creds"],400);
            }
        }catch (JWTException $e){
            return response()->json(["error"=>"Could not create creds"],500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser(){
        return response()->json(compact('user'));
    }
}
