<?php

namespace App\Http\Controllers\Api;

use App\Models\Events;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class EventController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $time = \Carbon\Carbon::now()->timestamp;
        $data = Events::where('event_start_data','>=',$time);
        return response()->json(["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
       try{
           $validator = Validator::make($request->all(),[
               'event_name'=>'require|max:255',
               'conducted_by'=>'require|max:255',
               'seats_available'=>'require',
               'event_start_data'=>'require|default: null',
               'event_end_date'=>'require',
               'event_desc'=>'require|max:255',
               'event_file'=>'default:null',
               'event_image'=>'default:null',
               'created_by'=>'require'
           ]);

           if ($validator->fails()){
               return response()->json($validator->errors(),400);
           }

           $events = new Events();
           $events->event_id = Uuid::uuid4();
           $events->event_name = $request->get('event_name');
           $events->conducted_by = $request->get('conducted_by');
           $events->seats_available = $request->get('seats_available');
           $events->event_start_date = $request->get('event_start_date');
           $events->event_end_date = $request->get('event_end_date');
           $events->event_desc = $request->get('event_desc');
           $events->event_file = $request->get('event_file');
           $events->event_image = $request->get('event_image');
           $events->created_by = $request->get('created_by');
           $events->save();
           return response()->json(["data"=>$events],200);
       }catch (Exception $e){
           return response()->json(["error"=>$e->getMessage(),"code"=>$e->getCode()],400);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Events::where('event_id','=',$id)->firstOrFail;
        return response()->json(["data"=>$result],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
