<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Initial_Survey_Response extends Model
{
    use Notifiable;
    protected $table = "initial_survey_responses";
}
