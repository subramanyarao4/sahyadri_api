<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User_Notification extends Model
{
    use Notifiable;
    protected $table = "user_notification";
}
