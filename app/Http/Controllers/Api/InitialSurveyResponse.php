<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Initial_Survey_Response;
use Illuminate\Support\Facades\Validator;

class InitialSurveyResponse extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = Initial_Survey_Response::all();
        return response()->json (['data'=>$answers],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                "question_id"=>'required|max:255',
                "user_id"=>'required|max:255',
                "response"=>'required|max:255'
            ]);
            if($validator->fails()){
                return response()->json(['error'=>$validator->errors()],400);
            }
            $initQuestion =  new Initial_Survey_Response();
            $initQuestion->question_id = $request->get('question_id');
            $initQuestion->user_id = $request->get('user_id');
            $initQuestion->response = $request->get('response');
            $initQuestion->save();
            return response()->json(204);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage(),'code'=>$e->getCode()],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
