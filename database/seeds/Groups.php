<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class Groups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */

    public function run()
    {
        DB::table('groups')->insert(
            [
                'group_id' => Uuid::uuid4(),
                'group_type' => "Admin"
            ]
        );
    }
}
