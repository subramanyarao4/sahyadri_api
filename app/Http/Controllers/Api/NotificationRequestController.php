<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification_Request;
use App\Models\User_Notification;
use App\Models\UserDetails;
use App\Models\ProjectCollaboration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = [];
        $userId =  Auth::user()->getAuthIdentifier();
        $data = UserDetails::where("firebaseUid", "=", $userId)->get('user_id');
        $notifyData = Notification_Request::all();
        // $notifyData[$i]['notification_id']$data[0]['user_id']
        for($i=0;$i<count($notifyData);$i++){
            $values = User_Notification::where(['user_id' =>$data[0]["user_id"],'notification_id'=>$notifyData[$i]["notification_id"]])->get();
            if(count($values)==0){
                 $arr = DB::table('notification_request')->join('projects','notification_request.project_id','=','projects.project_id')->where("request_user_id","!=",$data[0]['user_id'])->where("notification_id","=",$notifyData[$i]["notification_id"])->get();
                array_push($notifications,$arr);
            }else{
                continue;
            }
        }
        // $notification = DB::table('notification_request')->join('user_notification', 'notification_request.notification_id', '=', 'user_notification.notification_id')->where('request_user_id','=',$data[0]['user_id'])->where('is_accepted','=',false);
       return response()->json(json_encode(['data' => $notifications]), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notification = new User_Notification();
        $notification->notification_id = $id;
        $notification->user_id = $request->get('user_id');
        $notification->is_accepted = $request->get('is_accepted');
        $notification->created_at = \Carbon\Carbon::now()->timestamp;
        $notification->updated_at = \Carbon\Carbon::now()->timestamp;
        $notification->save();
        $projectGroups = new ProjectCollaboration();
        $projectGroups->group_id = '9d2c6505-59b5-41d7-94df-1acf320a95d1';
        $projectGroups->user_id = $request->get('user_id');
        $projectGroups->project_id =  $request->get('project_id');
        $projectGroups->save();
        return response()->json(204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
