<?php

namespace App\Providers;

use App\Helpers\FirebaseAuthProvider;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Kreait\Firebase\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        app('Dingo\Api\Auth\Auth')->extend('jwt', function ($app) {
            return new \Dingo\Api\Auth\Provider\JWT($app['Tymon\JWTAuth\JWTAuth']);
        });

        app('Dingo\Api\Auth\Auth')->extend('firebase', function ($app) {
            return new FirebaseAuthProvider($app['Firebase\Auth\Token\Verifier']);
        });
        $this->app['auth']->viaRequest('firebase', function ($request) {
            return app(\csrui\LaravelFirebaseAuth\Guard::class)->user($request);
        });
    }
}
